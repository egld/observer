#!/usr/bin/env python
# coding: utf-8

# In[468]:


from abc import ABC, abstractmethod
class AbstractClient(ABC):
    @abstractmethod
    def update(self,client, state):
        pass


# In[469]:


class Client(AbstractClient):
    def __init__(self, name):
        self.name = name
        
    def update(self,client, state):
        print(f"{client} {state}") 


# In[470]:


class Shop:
    def __init__(self):
        self.clients = set()
        
    def addClient(self, client):
        self.clients.add(client)
         
    def removeClient(self, client):
        self.clients.remove(client)

    def notifyClients(self):
        for client in self.clients:
            client.update(client.name,"Вы с нами")


# In[471]:


vasya = Client("vasya")
petr = Client("petr")
shop = Shop()
shop.addClient(vasya)
shop.addClient(petr)
shop.notifyClients()
shop.removeClient(petr)
print("!!!!!!!!!!!!!!!!!!!!!!")
shop.notifyClients()


# In[ ]:





# In[ ]:




